import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesS2Component } from './recipes-s2.component';

describe('RecipesS2Component', () => {
  let component: RecipesS2Component;
  let fixture: ComponentFixture<RecipesS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecipesS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
