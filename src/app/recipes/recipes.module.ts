import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipesRoutingModule } from './recipes-routing.module';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipesS1Component } from './recipes-s1/recipes-s1.component';
import { RecipesS2Component } from './recipes-s2/recipes-s2.component';


@NgModule({
  declarations: [
    RecipesComponent,
    RecipesS1Component,
    RecipesS2Component
  ],
  imports: [
    CommonModule,
    RecipesRoutingModule
  ]
})
export class RecipesModule { }
