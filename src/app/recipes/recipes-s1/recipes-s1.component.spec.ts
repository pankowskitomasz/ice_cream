import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesS1Component } from './recipes-s1.component';

describe('RecipesS1Component', () => {
  let component: RecipesS1Component;
  let fixture: ComponentFixture<RecipesS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecipesS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
