import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchesRoutingModule } from './branches-routing.module';
import { BranchesComponent } from './branches/branches.component';
import { BranchesS1Component } from './branches-s1/branches-s1.component';
import { BranchesS2Component } from './branches-s2/branches-s2.component';


@NgModule({
  declarations: [
    BranchesComponent,
    BranchesS1Component,
    BranchesS2Component
  ],
  imports: [
    CommonModule,
    BranchesRoutingModule
  ]
})
export class BranchesModule { }
