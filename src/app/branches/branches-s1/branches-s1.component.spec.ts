import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesS1Component } from './branches-s1.component';

describe('BranchesS1Component', () => {
  let component: BranchesS1Component;
  let fixture: ComponentFixture<BranchesS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BranchesS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
