import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchesS2Component } from './branches-s2.component';

describe('BranchesS2Component', () => {
  let component: BranchesS2Component;
  let fixture: ComponentFixture<BranchesS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BranchesS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
